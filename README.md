# GTK+ and GNOME Shell Theme Starter

A starter package and documentation on how to get started on GTK+ and GNOME Shell themes.

## Installing & Using

After modifying the theme, you can install it through meson.

- meson build --prefix=/usr
- sudo ninja -C build install

Be sure to add any new files into the appropriate meson.build folder. You can learn more about
that in the (coming) tutorial/documentation files.

# TODO: CREDITS
https://github.com/godlyranchdressing/gnome-theme-starter
Adwaita GTK theme