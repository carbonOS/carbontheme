with import <nixpkgs> {};
stdenv.mkDerivation rec {
	name = "cSH-theme-${version}";
	version = "0.0.0";
	
	src = ./.;
	nativeBuildInputs = [ meson ninja sassc ];
}